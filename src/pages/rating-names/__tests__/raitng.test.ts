import { test as base, expect } from '@playwright/test';
import { serverErrorResponse } from '../__mocks__/serverErrorResponse';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {

  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        headers: {
          'Content-Type': 'application/json',
        },
        ...serverErrorResponse,
      });
    }
  );

  await ratingPage.openRatingPage();

  await expect(page.locator(ratingPage.errorBlockSelector)).toBeVisible();
});

test('Рейтинг котиков отображается', async ({ page, ratingPage }) => {

  await ratingPage.openRatingPage();

  const likes = await ratingPage.getLikes();
  await ratingPage.checkLikes(likes);
});
