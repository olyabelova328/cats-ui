export const successResponse = {
  groups: [
    {
      title: 'А',
      cats: [
        {
          id: 17883,
          name: 'Аааа',
          description: '',
          tags: null,
          gender: 'unisex',
          likes: 2,
          dislikes: 0,
          count_by_letter: '18',
        },
      ],
      count_in_group: 1,
      count_by_letter: 18,
    },
  ],
  count_output: 1,
  count_all: 1,
};
